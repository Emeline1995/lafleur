class Article {
  int id;
  String nom;
  int category;
  var prix;
  String magasin;
  String image;

  Article();

  //configurer article grâce un map
  void fromMap(Map<String, dynamic> map) {
    this.id = map['id'];
    this.nom = map['nom'];
    this.category = map['category'];
    this.prix = map['prix'];
    this.magasin = map['magasin'];
    this.image = map['image'];
  }

  //Convertir article en map
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'nom': this.nom,
      'category': this.category,
      'prix': this.prix,
      'magasin': this.magasin,
      'image': this.image,
    };
    if (id != null) {
      map['id'] = this.id;
    }
    return map;
  }
}
