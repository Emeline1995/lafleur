import 'dart:io';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'category.dart';
import 'article.dart';

class DatabaseClass {
  //déclaration en private de la variable _database de type Database
  Database _database;

  //fonction d'accés à la variable _database
  Future<Database> get database async {
    if (_database != null) {
      return _database;
    } else {
      //créer base de données
      _database = await create();
      return _database;
    }
  }

  // fonction creation de la base de données
  Future create() async {
    Directory directory = await getApplicationDocumentsDirectory();
    //récupération du chemin vers le dossier de mes documents et ajout de 'database.db'
    String database_directory = join(directory.path, 'database.db');
    var bdd =
        await openDatabase(database_directory, version: 1, onCreate: _onCreate);
    return bdd;
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
     CREATE TABLE category (
     id INTEGER PRIMARY KEY,
     nom TEXT NOT NULL)
     ''');
    await db.execute('''
     CREATE TABLE article (
     id INTEGER PRIMARY KEY,
     nom TEXT NOT NULL, 
     category INTEGER,
     prix TEXT,
     magasin TEXT, 
     image TEXT
     )
     ''');
  }

  /* Ecriture des données */
  Future<Category> addCat(Category category) async {
    //vérifie base de donnée existe
    Database maDatabase = await database;
    //renvoi un int = id
    category.id = await maDatabase.insert('category', category.toMap());
    return category;
  }

  //modifier
  Future<int> updateCategory(Category category) async {
    Database maDatabase = await database;
    return await maDatabase.update('category', category.toMap(),
        where: 'id = ?', whereArgs: [category.id]);
  }

  //update or insert Category
  Future<Category> upsertCategory(Category category) async {
    Database maDatabase = await database;
    if (category.id == null) {
      //
      category.id = await maDatabase.insert('category', category.toMap());
    } else {
      await maDatabase.update('category', category.toMap(),
          where: 'id = ?', whereArgs: [category.id]);
    }
    return category;
  }

  //update or insert Article
  Future<Article> upsertArticle(Article article) async {
    Database maDatabase = await database;
    (article.id == null)
        ? article.id = await maDatabase.insert('article', article.toMap())
        : await maDatabase.update('article', article.toMap(),
            where: 'id = ?', whereArgs: [article.id]);
    return article;
  }

  //supprimer
  Future<int> delete(int id, String table) async {
    Database maDatabase = await database;
    await maDatabase.delete('article', where: 'category = ?', whereArgs: [id]);
    return await maDatabase.delete(table, where: 'id = ?', whereArgs: [id]);
  }

// LECTURE
  Future<List<Category>> allCategory() async {
    Database maDatabase = await database;
    List<Map<String, dynamic>> resultat =
        await maDatabase.rawQuery('SELECT * FROM category');
    List<Category> categories = [];
    resultat.forEach((map) {
      Category category = new Category();
      category.fromMap(map);
      categories.add(category);
    });
    return categories;
  }

  Future<List<Article>> allArticle(int category) async {
    Database maDatabase = await database;
    List<Map<String, dynamic>> resultat =
    await maDatabase.query('article', where: 'category = ?', whereArgs: [category]);
    List<Article> articles = [];
    resultat.forEach((map) {
      Article article = new Article();
      article.fromMap(map);
      articles.add(article);
    });
    return articles;
  }
}
