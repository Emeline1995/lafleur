import 'package:flutter/material.dart';
import 'dart:async';
import 'package:caslafleur_app/models/category.dart';
import 'package:caslafleur_app/widgets/voidData.dart';
import 'package:caslafleur_app/models/databaseClass.dart';
import 'package:caslafleur_app/widgets/detailCategory.dart';
class HomeController extends StatefulWidget {
  HomeController({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomeControllerState createState() => new _HomeControllerState();
}

class _HomeControllerState extends State<HomeController> {
  String nouvelleCategorie;
  List<Category> categories;
  @override
  void initState(){
    super.initState();
    recuperer();
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.title),
          actions: <Widget>[
            new FlatButton(
                onPressed: (() => ajouter(null)),
                child: new Text("Ajouter",
                    style: new TextStyle(color: Colors.white)))
          ],
        ),
        body: (categories == null || categories.length == 0)
            ? new voidData()
            : new ListView.builder(
                itemCount: categories.length,
                itemBuilder: (context, i) {
                  Category category = categories[i];
                  return new ListTile(
                    title: new Text(category.nom),
                    trailing: new IconButton(
                        icon: new Icon(Icons.delete),
                        onPressed: (){
                          DatabaseClass().delete(category.id, 'category').then((int){
                            recuperer();
                          }); 
                        }),//IconButton
                    leading: new IconButton(icon: new Icon(Icons.edit), onPressed: (() => ajouter(category))),
                    onTap: (){
                      Navigator.push(context, new MaterialPageRoute(builder: (BuildContext buildContext){
                        return new DetailCategory(category);
                      }));
                    },
                  ); //ListTile
                }) //ListView.builder
        ); //Scaffold
  }

  Future<Null> ajouter(Category category) async {
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext buildContext) {
          return new AlertDialog(
            title: new Text('Ajouter une catégorie'),
            content: new TextField(
              decoration: new InputDecoration(
                labelText: "categorie :",
                hintText: (category == null) ? "ex: Roses" : category.nom,
              ), //InputDecoration
              onChanged: (String str) {
                nouvelleCategorie = str;
              },
            ), //TextField
            actions: <Widget>[
              new FlatButton(
                  onPressed: (() => Navigator.pop(buildContext)),
                  child: new Text('Annuler')),
              new FlatButton(
                  onPressed: () {
                    //Ajouter à la base de données
                    if(nouvelleCategorie != null){
                      if (category == null){
                       category = new Category();
                       Map<String, dynamic> map = {'nom' : nouvelleCategorie};
                       category.fromMap(map);
                      }else{
                        category.nom = nouvelleCategorie;
                      }
                      DatabaseClass().upsertCategory(category).then((i) => recuperer());
                      nouvelleCategorie = null;
                    }
                    Navigator.pop(buildContext);
                  },
                  child: new Text(
                    'Valider',
                    style: new TextStyle(color: Colors.blue),
                  )) //FlatButton
            ], //<Widget>[]
          ); //AlertDialog
        });
  }

  void recuperer(){
    DatabaseClass().allCategory().then((categories){
      setState(() {
        this.categories = categories;
      });
    });
  }
}
