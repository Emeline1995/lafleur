import 'package:flutter/material.dart';
import 'package:caslafleur_app/models/category.dart';
import 'package:caslafleur_app/models/article.dart';
import 'package:caslafleur_app/widgets/VoidData.dart';
import 'package:caslafleur_app/widgets/addArticle.dart';
import 'package:caslafleur_app/models/databaseClass.dart';
import 'dart:io';

class DetailCategory extends StatefulWidget {
  Category category;
  DetailCategory(Category category) {
    this.category = category;
  }

  @override
  State<StatefulWidget> createState() => new _DetailCategoryState();
}

class _DetailCategoryState extends State<DetailCategory> {
  List<Article> articles;
  @override
  void initState() {
    super.initState();
    DatabaseClass().allArticle(widget.category.id).then((liste) {
      setState(() {
        articles = liste;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.category.nom),
        actions: <Widget>[
          new FlatButton(
              onPressed: () {
                Navigator.push(context,
                    new MaterialPageRoute(builder: (BuildContext context) {
                  return new AddArticle(widget.category.id);
                })).then((value) {
                  DatabaseClass().allArticle(widget.category.id).then((liste) {
                    setState(() {
                      articles = liste;
                    });
                  });
                });
              },
              child: new Text(
                'Ajouter',
                style: new TextStyle(color: Colors.white),
              )),
        ], //<<Widget>[]
      ), //AppBar
      body: (articles == null || articles.length == 0)
          ? new voidData()
          : new GridView.builder(
              itemCount: articles.length,
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),
              itemBuilder: (context, i) {
                Article article = articles[i];
                return new Card(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new Text(
                        article.nom,
                        textScaleFactor: 1.4,
                      ),
                      new Container(
                        height: MediaQuery.of(context).size.height / 2.5,
                        child: (article.image == null)
                            ? new Image.asset('images/no_image.jpg')
                            : new Image.file(new File(article.image)),
                      ),
                      new Text((article.prix == null)
                          ? 'Aucun prix renseigné'
                          : "Prix: ${article.prix}"),
                      new Text((article.magasin == null)
                          ? 'Aucun magasin renseigné'
                          : "Magasin: ${article.magasin}")
                    ], //<<Widget>[]
                  ), //Column
                ); //Card
              }), //GridView.builder
    ); //Scaffold
  }
}
